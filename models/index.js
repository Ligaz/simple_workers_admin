var DepartmentMeta = require('../meta/department'),
    WorkerMeta = require('../meta/worker'),
    TimesheetMeta = require('../meta/timesheet'),
    sequelize = require('../sequelize');

//Создаем нужные таблицы и настраиваем ассоциации
var Department = sequelize.define('departments', DepartmentMeta.attributes, DepartmentMeta.options);
var Worker = sequelize.define('workers', WorkerMeta.attributes, WorkerMeta.options);
var Timesheet = sequelize.define('timesheets', TimesheetMeta.attributes, TimesheetMeta.options);

Worker.hasMany(Timesheet,{onDelete:'cascade'});
Timesheet.belongsTo(Worker,{as:'worker',contraints:false});
Department.hasMany(Worker,{ onDelete: 'cascade' });
Worker.belongsTo(Department,{ as: 'department', constraints: false });

sequelize.sync();

module.exports.Department = Department;
module.exports.Timesheet = Timesheet;
module.exports.Worker = Worker;