var LocalStrategy = require('passport-local').Strategy,
    Worker = require('../models').Worker,
    passwordHash = require('password-hash');

//Стратегия локальной авторизации пользователя
var Strategy = new LocalStrategy(function(username, password, done) {
    Worker.findOne({
        where: {
            'login': username
        }
    }).then(function (user) {
        if (user == null) {
            return done(null, false, {message: 'Incorrect credentials.'})

        }
        if (passwordHash.verify(password, user.dataValues.password)) {
            return done(null, user);
        }
        return done(null, false, {message: 'Incorrect credentials.'})
    })
});

module.exports = Strategy;
