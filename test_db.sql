-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 31 2017 г., 21:58
-- Версия сервера: 5.5.50
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `departments`
--

INSERT INTO `departments` (`id`, `title`) VALUES
(19, 'Бухгалтерия'),
(21, 'Топ менеджмент'),
(22, 'Отдел разработки'),
(24, 'Отдел продаж');

-- --------------------------------------------------------

--
-- Структура таблицы `timesheets`
--

CREATE TABLE IF NOT EXISTS `timesheets` (
  `id` int(11) NOT NULL,
  `day` datetime NOT NULL,
  `startTime` datetime NOT NULL,
  `endTime` datetime NOT NULL,
  `workerId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `timesheets`
--

INSERT INTO `timesheets` (`id`, `day`, `startTime`, `endTime`, `workerId`) VALUES
(1, '2017-05-31 21:00:00', '2017-05-31 05:03:00', '2017-05-31 10:06:00', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `workers`
--

CREATE TABLE IF NOT EXISTS `workers` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `birthday` datetime NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `departmentId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `workers`
--

INSERT INTO `workers` (`id`, `firstName`, `lastName`, `login`, `password`, `birthday`, `gender`, `createdAt`, `updatedAt`, `departmentId`) VALUES
(2, 'Игорь', 'Васильев', 'test1', 'sha1$be3bfa24$1$3a100dc55935da80cfd2cc18b50d589f29bea25a', '1996-08-04 21:00:00', 'Мужской', '2017-05-31 18:39:17', '2017-05-31 18:39:17', 24),
(3, 'Вячеслав', 'Селиванов', 'tes2', 'sha1$e5c8f667$1$3f59511ae17afa4a42f7824b78ef989eb27566a7', '1984-01-08 22:00:00', 'Мужской', '2017-05-31 18:40:00', '2017-05-31 18:40:00', 19),
(4, 'Юрий', 'Краснов', 'test1', 'sha1$845cc8cb$1$537631b8ac993178e79c2837731d652ef098a645', '1981-01-11 22:00:00', 'Мужской', '2017-05-31 18:40:42', '2017-05-31 18:40:42', 24),
(5, 'Ирина', 'Никитина', 'test4', 'sha1$312ff179$1$d33d2e0cbc2f5660d0330f3308c64ab4535e6136', '1982-02-21 22:00:00', 'Женский', '2017-05-31 18:41:24', '2017-05-31 18:41:24', 19),
(6, 'Валентин', 'Арбузов', 'test5', 'sha1$9bed67f0$1$4f1c253be2ab54082dbf1890d9391e7e6ec4304f', '1993-01-02 22:00:00', 'Мужской', '2017-05-31 18:45:10', '2017-05-31 18:45:10', 21),
(7, 'Наталья', 'Балотная', 'test7', 'sha1$fad42aeb$1$26e46cf3095fab0a7156c3b93308d4b3885f91bf', '1977-07-03 21:00:00', 'Женский', '2017-05-31 18:46:07', '2017-05-31 18:46:07', 22),
(8, 'Анастасия', 'Гринева', 'test8', 'sha1$7a919907$1$66e3150c2cd94a76bd8b3a6f4d3c139bc47fc275', '2001-07-08 21:00:00', 'Женский', '2017-05-31 18:46:50', '2017-05-31 18:46:50', 22),
(9, 'Иван', 'Никифоров', 'test9', 'sha1$e2a6cb0f$1$44dcca2e14c86c2a99ffa8c8e75f38c98c1ece1e', '1972-12-31 22:00:00', 'Мужской', '2017-05-31 18:47:51', '2017-05-31 18:47:51', 24),
(10, 'Юрий', 'Дудин', 'test10', 'sha1$df7feb42$1$aa23fdd88b331c595ddcf6a78e0ddec417003084', '1982-01-03 22:00:00', 'Мужской', '2017-05-31 18:48:45', '2017-05-31 18:48:45', 21),
(11, 'Ирина', 'Григорец', 'test11', 'sha1$24d544c5$1$f65173a27ded756bd7ebfa33e1403f76e097592c', '1995-07-02 21:00:00', 'Женский', '2017-05-31 18:49:29', '2017-05-31 18:49:29', 22),
(12, 'Виталий', 'Максимов', 'test13', 'sha1$9257a60d$1$47271ae6fe23651a611e955b405b7839b57d2245', '1993-02-08 22:00:00', 'Мужской', '2017-05-31 18:50:30', '2017-05-31 18:50:30', 22);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `timesheets`
--
ALTER TABLE `timesheets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workerId` (`workerId`);

--
-- Индексы таблицы `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departmentId` (`departmentId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `timesheets`
--
ALTER TABLE `timesheets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `timesheets`
--
ALTER TABLE `timesheets`
  ADD CONSTRAINT `timesheets_ibfk_1` FOREIGN KEY (`workerId`) REFERENCES `workers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `workers`
--
ALTER TABLE `workers`
  ADD CONSTRAINT `workers_ibfk_1` FOREIGN KEY (`departmentId`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
