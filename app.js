'use strict'
var express = require('express'),
    app = express(),
    path = require('path'),
    router = require('./router/index'),
    api = require('./router/api'),
    bodyParser = require('body-parser'),
    config = require(path.resolve('config')),
    cookieParser = require('cookie-parser'),
    passport = require('passport'),
    Strategy = require('./passport/passport'),
    Models = require('./models');

//Настраиваем bodyPareser, даем доступ к статическим файлам и подключаем сессии
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/dist',express.static(path.join(__dirname,"dist")));
app.use(require('express-session')({
    secret: 'super secret',
    resave: false,
    saveUninitialized: false
}));
app.use(cookieParser());

//Блок настройки аутентификации пользователей
app.use(passport.initialize());
app.use(passport.session());
passport.use(Strategy);
passport.serializeUser(function(user, done){
    done(null, user);
});
passport.deserializeUser(function(obj, done){
    done(null, obj);
});

//Подключаем обработчики роутов
app.use("/api",api);
app.use("/",router);

//Обработчик для некорректных запросов
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//Обработчик ошибок
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.log(err);
    res.sendfile('dist/templates/error.html');
});

//Стартуем сервер
app.listen(config.get("connection:port"),function () {
    console.log('Application listening on port ' + config.get("connection:port"));
});


