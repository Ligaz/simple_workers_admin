### Функционал приложения

**Неавторизированный пользователь**

- Может просматривать информацию о сотрудниках, отделах и табельном времени
- Выполнять любую сортировку и фильтрацию

**Авотризированный пользователь**

- Может добавлять новых сотрудников, изменять их данные
- Добавлять и изменять подразделения
- Добавлять свои метки табельного времени

### Установка и запуск

Для корректной работы приложения необходимо:

- Создать пользователя бд и базу MySQL (версия сервера не ниже 5.5) с именем **test_db** (пароль тот же)
- Импортировать дамп бд из файла **test_db.sql** (находится в корне данного репозитория)
- Запустить из директории проекта установку зависимостей npm командой **npm install --production** (чтоб не тянуть зависимости для девелопмента)
- Стартовать проект нужно с файла app.js, командой **node app.js**
- После появления лога **Application listening on port...** можно заходить на localhost:номер_порта и смотреть приложение, логин для авторизации в приложении **test1**, пароль **test**


 
