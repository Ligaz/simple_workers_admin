var express = require('express');
var router = express.Router();
var path = require('path');

//Роут для индекса в SPA
router.get('*', function(req, res, next) {
    res.sendFile(path.resolve('dist/templates/index.html'));
});


module.exports = router;
