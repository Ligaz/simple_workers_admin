var express = require('express');
var router = express.Router();
var path = require('path');
var passwordHash = require('password-hash');
var passport = require('passport');
var Worker = require('../models').Worker;
var Department = require('../models').Department;
var Timesheet = require('../models').Timesheet;

//Блок роутов для работы с подразделениями
router.get('/departments', function (req, res, next) {
    Department.findAll({
        order: [['title', 'ASC']]
    })
        .then(
            function (result) {
                res.json(result);
            }
        )
});
//Проверка подразделения на наличие в нём сотрудников
router.get('/checkIsUsed/departments/:id', function (req, res, next) {
    Worker.findOne({where: {departmentId: req.params.id}})
        .then(
            function (result) {
                if (result) {
                    res.end('yes');
                } else {
                    res.end('no');
                }
            }
        )
});
//Получаем количество сотрудников по id подразделения
router.get('/checkCount/departments/:id', function (req, res, next) {
    Worker.count({where: {departmentId: req.params.id}})
        .then(
            function (result) {
                res.end(result.toString());
            }
        )
});
router.post('/departments', function (req, res, next) {
    if (req.isAuthenticated()) {
        Department.create(req.body)
            .then(
                function (result) {
                    res.end('ok');
                }
            )
    } else {
        res.statusCode = 401;
        res.end('Unauthorized');
    }
});
router.put('/departments', function (req, res, next) {
    if (req.isAuthenticated()) {
        Department.findById(req.body.id)
            .then(
                function (department) {
                    department.update(req.body);
                    res.end('ok');
                }
            )
    } else {
        res.statusCode = 401;
        res.end('Unauthorized');
    }
});
router.delete('/departments/:id', function (req, res, next) {
    if (req.isAuthenticated()) {
        Department.destroy({where: {id: req.params.id}})
            .then(function () {
                res.end('ok');
            });
    } else {
        res.statusCode = 401;
        res.end('Unauthorized');
    }
});
// Блок авторизации и аутентификации
router.post('/login', passport.authenticate('local'), function (req, res, next) {
    res.json(req.user.dataValues);
});
router.get('/logout', function (req, res) {
    if (req.isAuthenticated()) {
        req.logout();
        res.end('ok');
    } else {
        res.end('Unauthorized');
    }
});
router.get('/verify', function (req, res, next) {
    if (req.isAuthenticated()) {
        res.json(req.session.passport.user);
    } else {
        res.statusCode = 401;
        res.end("Unauthorized");
    }
});
//Блок манипуляции данными сотрудников
router.get('/workers/:options', function (req, res, next) {
    //Из параметров роута получаем опции для выборки из базы, проверяем их
    var options = JSON.parse(req.params.options);
    var mod_options = {include: [{model: Department, as: 'department'}], where: {}};
    if (options.limit) {
        mod_options.limit = options.limit;
    }
    if (options.sortBy) {
        mod_options.order = [[options.sortBy, options.sortType]]
    }
    if (options.gender != 'none') {
        mod_options.where.gender = options.gender;
    }
    if (options.department != 'none') {
        mod_options.where.departmentId = options.department;
    }
    if (options.page) {
        mod_options.offset = (options.page - 1) * options.limit;
    }
    Worker.findAndCountAll(
        mod_options)
        .then(
            function (result) {
                res.json(result);
            },
            function (err) {
                console.log(err);
            }
        )
});

router.post('/workers', function (req, res, next) {
    if (req.isAuthenticated()) {
        var worker = req.body;
        worker.departmentId = worker.department.id;
        worker.password = passwordHash.generate(worker.password);
        Worker.create(worker)
            .then(
                function (result) {
                    res.end('ok');
                },
                function (err) {
                    res.statusCode = 500;
                    res.end();
                    console.log(err);
                }
            )
    } else {
        res.statusCode = 401;
        res.end('Unauthorized');
    }

});
router.put('/workers', function (req, res, next) {
    if (req.isAuthenticated()) {
        Worker.findById(req.body.id)
            .then(
                function (worker) {
                    var mod_worker = req.body;
                    mod_worker.departmentId = mod_worker.department.id;
                    mod_worker.password = passwordHash.generate(mod_worker.password);
                    worker.update(mod_worker);
                    res.end('ok');
                },
                function (err) {
                    res.statusCode = 500;
                    res.end();
                    console.log(err);
                }
            )
    } else {
        res.statusCode = 401;
        res.end('Unauthorized');
    }
});
router.delete('/workers/:id', function (req, res, next) {
    if (req.isAuthenticated()) {
        Worker.destroy({where: {id: req.params.id}})
            .then(function () {
                    res.end('ok');
                },
                function (err) {
                    res.statusCode = 500;
                    res.end();
                    console.log(err);
                });
    } else {
        res.statusCode = 401;
        res.end('Unauthorized');
    }
});


router.post('/timesheet', function (req, res, next) {
    if (req.isAuthenticated()) {
        var data = req.body;
        data.workerId = req.session.passport.user.id;
        var date = new Date(data.day);
        date.setDate(date.getDate() + 1);
        data.day = date;
        Timesheet.create(data)
            .then(
                function (result) {
                    res.end('ok');
                },
                function (err) {
                    res.statusCode = 500;
                    res.end();
                    console.log(err);
                }
            );
    } else {
        res.statusCode = 401;
        res.end("Unauthorized");
    }
});
router.get('/timesheet/:date', function (req, res, next) {
    var date = new Date(req.params.date);
    date.setDate(date.getDate() + 1);
    Timesheet.findAll({
        include: [{model: Worker, attributes: ['firstName', 'lastName'], as: 'worker'}],
        where: {day: date}
    })
        .then(
            function (result) {
                res.json(result);
            },
            function (err) {
                res.statusCode = 500;
                res.end();
                console.log(err);
            }
        );
});

module.exports = router;
