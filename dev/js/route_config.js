'use strict'

angular.module('app')//Создаем файл конфигурации роутинга приложения и разделения по view
    .config(function($routeProvider,$routeSegmentProvider,$locationProvider){
        $routeSegmentProvider
            .when('/workers/page=:page','workers')
            .when('/reports','reports')
            .when('/departments','departments')
            .when('/error','error');
        $routeSegmentProvider.segment('workers',{
            templateUrl:'dist/templates/segments/workers.html',
            controller:'WorkersCtrl',
            resolve: {
                loadWorkersList: function ($rootScope,WorkerService,DepartmentService,$q,$routeParams) {
                    $rootScope.storage.options.page = parseInt($routeParams.page);
                    return $q.all([WorkerService.getWorkers($rootScope,$rootScope.storage.options),DepartmentService.getDepartments($rootScope)]);
                }
            }
        });
        $routeSegmentProvider.segment('reports',{
            templateUrl:'dist/templates/segments/reports.html',
            controller:'TimestampCtrl',
            resolve:{
                clearReportsList: function ($rootScope) {
                    $rootScope.reportsList = null;
                }
            }
        });
        $routeSegmentProvider.segment('departments',{
            templateUrl:'dist/templates/segments/departments.html',
            controller:'DepartmentsCtrl',
            resolve:{
                loadDepartments: function ($rootScope,DepartmentService) {
                    DepartmentService.getDepartments($rootScope);
                }
            }
        });
        $routeProvider.otherwise({redirectTo:'/workers/page=1'});//При любом другом URL попадаем на страницу ошибки
        $locationProvider.html5Mode(true);//Убираем хештеги при роутинге
    });
