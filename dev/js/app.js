'use strict'
var app = angular.module('app',['ngMaterial','ctrls','directives','ngRoute','ngStorage', 'route-segment', 'view-segment','services','ngAnimate','mdPickers']);

app.run(function ($rootScope,$routeSegment,$localStorage,UserService) {
    $rootScope.storage = $localStorage;//Подключаем local storage для хранения сортировки и фильтров на клиенте
    UserService.verify();//Аутентифицируем пользователя
    if (!$rootScope.storage.options) {//Задаем дефолтные настройки, если их нет
        $rootScope.storage.options = {department:'none',gender:'none',limit:5,sortBy:'firstName',sortType:'ASC'};
    }
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous, reject) {
        //Делаем перезагрузку данных если предыдущий и текущий роут одинаковы (необходимо для перезагрузки шаблона при пагинации)
        if (current) {
            if(current.$$route){
                var path = current.$$route.originalPath;
            }
            if(previous){
                if(previous.$$route){
                    var previousPath = previous.$$route.originalPath;
                }
                if(path==previousPath){
                    if($routeSegment){
                        $routeSegment.chain[0].reload();
                    }
                }
            }
        }
    });
});
