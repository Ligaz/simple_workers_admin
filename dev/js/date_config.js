angular.module('app').config(function($mdDateLocaleProvider) {
    //Конфигурируем формат даты для вывода в mdDatePicker
    $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('D.MM.YYYY');
    };
});
