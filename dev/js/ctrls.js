var controllers = angular.module('ctrls', ['services']);

//Основной контроллер
controllers.controller('MainCtrl',function ($scope,$rootScope,$mdDialog,UserService){
    //Открытие формы авторизации
    $scope.openLoginForm = function (event) {
        $mdDialog.show({
            controller: 'MainCtrl',
            templateUrl: '/dist/templates/modals/login.html',
            parent: angular.element(document.body),
            targetEvent: event,
            multiple:true,
            clickOutsideToClose:true
        });
    };
    //Открытие формы для создания метки времени
    $scope.openTimestampForm = function (event) {
        $mdDialog.show({
            controller: 'TimestampCtrl',
            templateUrl: '/dist/templates/modals/timestamp.html',
            parent: angular.element(document.body),
            targetEvent: event,
            multiple:true,
            clickOutsideToClose:true
        });
    };
    //Функция авторизации
    $scope.login = function (user,valid) {
        if(valid){
            UserService.login(user)
                .then(
                    function (res) {
                        $scope.closeModal();
                    },
                    function (err) {
                        $scope.errorMessage = "Пользователь с таким логином и паролем не найден";
                    }
                )
        }else {
            $scope.errorMessage = "Введите логин и пароль";
        }

    };
    //Выход из системы
    $scope.logout = function () {
      UserService.logout();
    };
    $scope.closeModal = function () {
        $scope.errorMessage = null;
        $mdDialog.cancel();
    }
});
//Контроллер меток времени
controllers.controller('TimestampCtrl',function ($scope,$mdDialog,TimesheetService) {
    //Добавление метки
   $scope.addTimestamp = function (timestamp) {
       if(!timestamp||!timestamp.startTime||!timestamp.endTime){
           $scope.errorMessage = "Все поля должны быть заполнены";
       }else {
           //Проверяем валидность времени начала и конца работы
           var numberStartTime= timestamp.startTime.getTime();
           var numberEndTime = timestamp.endTime.getTime();
           if(numberEndTime<numberStartTime){
               $scope.errorMessage = "Время начала работы не может быть больше времени окончания";
           }else{
               //Устанавливаем нужный день для дат
               timestamp.startTime.setDate(timestamp.day.getDate());
               timestamp.endTime.setDate(timestamp.day.getDate());
               TimesheetService.addTimestamp(timestamp)
                   .then(
                       function (res) {
                           //Если метка добавлена, закрываем окно
                           if(res.data=="ok"){
                               $scope.closeModal();
                           }
                       }
                   )

           }
       }
   };
   //Вешаем вотчер на дату, чтобы отчет формировался при каждом изменении
     $scope.$watch('reportDate',function (newValue,oldValue) {
         if(newValue){
             TimesheetService.getTimestamps(newValue);
         }

     });
    $scope.closeModal = function () {
        $scope.errorMessage = null;
        $mdDialog.cancel();
    }
});
//Контроллер для работы с сортудниками
controllers.controller('WorkersCtrl',function ($scope,$mdDialog,DepartmentService,WorkerService,$rootScope,$location) {
    $scope.openEditWorkerForm = function (event,worker) {
        //Проверяем, это редактирование, или добавление нового сотрудника
        if(worker){
            $scope.isNew = false;
            $scope.currentWorker = worker;
        }else {
            $scope.isNew = true;
            $scope.currentWorker = null;
        }
        //Подгружаем список подразделений
        DepartmentService.getDepartments($scope)
            .then(
                function () {
                    $mdDialog.show({
                        controller: 'WorkersCtrl',
                        templateUrl: '/dist/templates/modals/edit_worker.html',
                        parent: angular.element(document.body),
                        targetEvent: event,
                        multiple:true,
                        clickOutsideToClose:true,
                        scope:$scope,
                        preserveScope:true
                    });
                }
            )

    };
    $scope.addWorker = function (worker,valid) {
        if(valid){
            var promise;
            if($scope.isNew){
                promise = WorkerService.addWorker(worker);
            }else{
                promise = WorkerService.updateWorker(worker);
            }
            promise.then(
                function (res) {
                    //Обновляем список после изменений
                    WorkerService.getWorkers($rootScope);
                    $scope.closeModal();
                },
                function (err) {
                    $scope.errorMessage = "Ошибка сервера, попробуйте повторить запрос позже";
                }
            )
        }else {
            $scope.errorMessage = "Ошибка при заполнении полей";
        }

    };
    $scope.deleteWorker = function (id) {
        WorkerService.deleteWorker(id)
            .then(
                function () {
                    WorkerService.getWorkers($rootScope);
                },
                function (err) {
                    $scope.errorMessage = "Ошибка сервера, попробуйте повторить запрос позже";
                }

            )
    };
    //Изменение сортировки и фильтрации, сбрасываем текущую страницу
    $scope.sorted = function () {
        $rootScope.storage.options.page = 1;
        $location.path('/workers/page='+$rootScope.storage.options.page);
        WorkerService.getWorkers($rootScope);
    };
    $scope.closeModal = function () {
        $scope.errorMessage = null;
        $mdDialog.cancel();
    }
});
//Контроллер для работы с подразделениями
controllers.controller('DepartmentsCtrl',function ($scope,DepartmentService,$mdDialog,$rootScope) {
    $scope.openEditDepForm = function (event,department) {
        if(department){
            $scope.isNew = false;
            $scope.currentDepartment = department;
        }else {
            $scope.isNew = true;
            $scope.currentDepartment = null;
        }
        $mdDialog.show({
            controller: 'DepartmentsCtrl',
            templateUrl: '/dist/templates/modals/edit_department.html',
            parent: angular.element(document.body),
            targetEvent: event,
            multiple:true,
            clickOutsideToClose:true,
            scope:$scope,
            preserveScope:true
        });
    };
    $scope.add = function (department,valid) {
        if(valid){
            var promise;
            if($scope.isNew){
                promise = DepartmentService.addDepartment(department);
            }else{
                promise = DepartmentService.updateDepartment(department);
            }

            promise.then(
                function (res) {
                    DepartmentService.getDepartments($rootScope);
                    $scope.closeModal();
                }
            )
        }else {
            $scope.errorMessage = "Введите название подразделения";
        }

    };
    $scope.delete = function (id) {
        DepartmentService.checkUsed(id)
            .then(
                function (result) {
                    if(result.data=='no'){
                        DepartmentService.deleteDepartment(id)
                            .then(
                                function (res) {
                                    if($rootScope.storage.options.department==id){
                                        $rootScope.storage.options.department='none';
                                    }
                                    DepartmentService.getDepartments($rootScope);
                                }
                            )
                    }else {
                        //Ложим id  в переменную, чтобы можно было продолжить операцию удаления
                        $scope.deleteItemId = id;
                        $mdDialog.show({
                            controller: 'DepartmentsCtrl',
                            templateUrl: '/dist/templates/modals/warning_delete.html',
                            parent: angular.element(document.body),
                            targetEvent: event,
                            multiple:true,
                            clickOutsideToClose:true,
                            scope:$scope,
                            preserveScope:true
                        });
                    }

                }
            )

    };
    //Функция каскадного удаления подразделения и его работников
    $scope.evenlyDelete = function () {
        DepartmentService.deleteDepartment($scope.deleteItemId)
            .then(
                function (res) {
                    if($rootScope.storage.options.department==$scope.deleteItemId){
                        $rootScope.storage.options.department='none';
                    }
                    DepartmentService.getDepartments($rootScope);
                    $scope.closeModal();
                }
            )
    }
    $scope.closeModal = function () {
        $scope.errorMessage = null;
        $mdDialog.cancel();
    }
});
