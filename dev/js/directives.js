var directives = angular.module('directives',['services']);
//Директива для асинхронной подгрузки информации о количестве сотрудников подразделения
directives.directive('ngCheckCount',function (DepartmentService) {
    return{
        restrict: 'A',
        link: function(scope,element,attrs){
            DepartmentService.checkCount(attrs.id)
                .then(
                    function (res) {
                        var count = res.data;
                        element.html(count);
                    }
                )
        }
    }
});
