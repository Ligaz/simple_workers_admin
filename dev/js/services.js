var services = angular.module('services',[]);

//Сервис для работы с REST API для сотрудников
services.factory('WorkerService',function ($http,$rootScope) {
    return {
       getWorkers: function (scope) {
          return $http.get('api/workers/'+JSON.stringify($rootScope.storage.options))
               .then(
                   function (res) {
                       $rootScope.workersList = {count:res.data.count,workers:res.data.rows};
                       //Создаем ссылки на страницы, если это нужно
                       $rootScope.pages = [];
                       if ($rootScope.workersList.count % $rootScope.storage.options.limit != 0) {
                           $rootScope.pagesCount = parseInt(($rootScope.workersList.count / $rootScope.storage.options.limit) + 1);
                       } else {
                           $rootScope.pagesCount = parseInt($rootScope.workersList.count / $rootScope.storage.options.limit);
                       }
                       for (var i = 1; i < $rootScope.pagesCount + 1; i++) {
                           $rootScope.pages.push(i);
                       }

                   },
                   function (err) {
                       alert('не удалось получить список работников');
                   }
               )
       },
        addWorker: function (worker) {
            return $http.post('/api/workers',worker);
        },
        updateWorker: function (worker) {
            return $http.put('/api/workers',worker);
        },
        deleteWorker: function (id) {
            return $http.delete('/api/workers/'+id);
        }
    }
});
//Сервис для работы с REST API для подразделений
services.factory('DepartmentService',function ($http) {
    return {
        getDepartments: function (scope) {
            return $http.get('api/departments')
                .then(
                    function (res) {
                        scope.departments = res.data;
                    },
                    function (err) {
                        alert('не удалось получить список подразделений');
                    }
                )
        },
        addDepartment: function (department) {
           return $http.post('/api/departments',department);
        },
        updateDepartment: function (department) {
            return $http.put('/api/departments',department);
        },
        deleteDepartment: function (id) {
            return $http.delete('/api/departments/'+id);
        },
        checkUsed: function (id) {
            return $http.get('/api/checkIsUsed/departments/'+id);
        },
        checkCount:function (id) {
            return $http.get('/api/checkCount/departments/'+id);
        }
    }
});
//Сервис авторизации
services.factory('UserService',function ($http,$rootScope) {
    return {
        login: function (user) {
            user.username = user.login;
            return $http.post('/api/login',user)
                .then(
                    function (res) {
                        $rootScope.user = res.data;
                    }
                )
        },
        logout: function () {
            return $http.get('/api/logout')
                .then(
                    function (res) {
                        if(res.data=='ok'){
                            $rootScope.user = null;
                        }
                    }
                )
        },
        verify: function () {
            $http.get('/api/verify')
                .then(
                    function (res) {
                        if(res.data=="Unauthorized"){
                            $rootScope.user = null;
                        }else {
                            $rootScope.user = res.data;
                        }
                    },function (err) {
                        console.log(err.data);
                    }
                )
        }
    }
});
//Сервис для работы с метками времени
services.factory('TimesheetService',function ($http,$rootScope) {
    return {
        addTimestamp: function (timestamp) {
            return $http.post('/api/timesheet',timestamp);
        },
        getTimestamps: function (reportDate) {
            $http.get('/api/timesheet/'+reportDate)
                .then(
                    function (res) {
                        $rootScope.reportsList = res.data;
                    }
                )
        }
    }
});