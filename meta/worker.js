var Sequelize = require('sequelize');

var attributes = {
    firstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    login: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.TEXT('longtext'),
        allowNull: false
    },
    birthday: {
        type: Sequelize.DATE,
        allowNull: false
    },
    gender: {
        type: Sequelize.STRING,
        allowNull: true
    }
};

var options = {
    freezeTableName: true,
    timestamps: true
};

module.exports.attributes = attributes;
module.exports.options = options;
